this.system = this.system || {};
(function(){
    "use strict";

    var CardDeck = function(){
        this.Container_constructor();
        this.initCardDeck();
    };

    var p = createjs.extend(CardDeck,createjs.Container);

    p._cardsArr = null;

    p.initCardDeck = function () {
        console.log("CardDeck Initialized...");

        var atlas = system.CustomMethods.makeImage("cardsAtlas" , true); // todo try with false
        var json = queue.getResult("cardsJson");

        this._cardsArr = [];
        var signs = ['Hearts' , 'Diamonds' , 'Clubs' , 'Spades'];
        for(var i = 2; i < 15; i++){ // numbers
            for(var j = 0; j < 4; j++){ // signs
                var cardName = "card" + signs[j] + i;
                var img = system.CustomMethods.makeImageFromAtlas(atlas , json , cardName);
                var card = new system.Card(i , signs[j] , img);
                this.addChild(card);
                this._cardsArr.push(card);
            }
        }
    };

    p.getCard = function (index) {
        return this._cardsArr[index];
    };

    system.CardDeck = createjs.promote(CardDeck,"Container");
})();