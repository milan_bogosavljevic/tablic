
this.system = this.system || {};
(function(){
    "use strict";

    var Game = function(){
        this.Container_constructor();
        this.initGame();
    };

    var p = createjs.extend(Game,createjs.Container);

    p._human = null;
    p._robot = null;
    p._cardsDeck = null;
    p._scoreTable = null;
    p._potCards = null;
    p._cardsArrIndexes = null;

    p.initGame = function () {
        console.log("Game Initialized...");
        var background = system.CustomMethods.makeImage("background" , false);
        this._cardsDeck = new system.CardDeck(); // container whitch contains all card containers
        this._scoreTable = new system.ScoreTable();
        this._human = new system.Human();
        this._robot = new system.Robot();
        this._potCards = []; // array of all possible cards on pot during the game
        this._cardsArrIndexes = []; // array for manipulation of card deck cards array

        this.addChild(background , this._cardsDeck);

        this.startRound();
    };

    p.startRound = function () {
        this.setCardsIndexes();
        this.cutDeck();
        this.dealCards();
    };

    p.setCardsIndexes = function() {
        this._cardsArrIndexes = [];
        for(var i = 0; i < 52; i++) {
            this._cardsArrIndexes.push(i);
        }
        this._cardsArrIndexes = system.CustomMethods.shuffleArr(this._cardsArrIndexes);
        console.log(this._cardsArrIndexes);
    };

    p.cutDeck = function() {
        var positions = [{x:800 , y:300} , {x:1000 , y:300} , {x:800 , y:500} , {x:1000 , y:500}];
        for(var i = 0; i < 4; i++){
            var cardIndex = this._cardsArrIndexes.pop();
            var card = this._cardsDeck.getCard(cardIndex);
            this._potCards.push(card);

            card.x = positions[i].x;
            card.y = positions[i].y;
        }
    };

    p.dealCards = function() {
        this.dealCardsTo("_human");
        this.dealCardsTo("_robot");
    };

    p.dealCardsTo = function (player) {
        var cards = [];
        for(var i = 0; i < 6; i++){
            var cardIndex = this._cardsArrIndexes.pop();
            var card = this._cardsDeck.getCard(cardIndex);
            cards.push(card);
        }
        this[player].setCardsInHands(cards);
    };

    p.render = function(e){
        //console.log("rendering");
        stage.update(e);
    };

    system.Game = createjs.promote(Game,"Container");
})();