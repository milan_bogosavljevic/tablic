this.system = this.system || {};
(function(){
    "use strict";

    var Human = function(){
        this.Player_constructor();
        this.initHuman();
    };

    var p = createjs.extend(Human , system.Player);

    p.initHuman = function () {
        console.log("Human Initialized...");
        this._yPos = 880;
    };

    system.Human = createjs.promote(Human , "Player");
})();