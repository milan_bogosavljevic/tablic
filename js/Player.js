this.system = this.system || {};
(function(){
    "use strict";

    var Player = function(){
        this.initPlayer();
    };

    var p = Player.prototype;

    p._cardsTakenArr = null;
    p._score = 0;
    p._cardsInHands = null;
    p._cardsInHandsPositions = null;
    p._startXPos = null;
    p._yPos = null;

    p.initPlayer = function () {
        console.log("Player Initialized...");
        this._cardsTakenArr = [];
        this._cardsInHands = [];

        this._startXPos = 500;
    };

    p.cardsTaken = function(cards) {
        for(var i = 0; i < cards.arr; i++){
            this._score += cards[i].getValue();
            this._cardsTakenArr.push(cards[i]);
        }
    };

    p.setCardsInHands = function(cards) {
        var spacing = 20;
        this._cardsInHands = cards;
        for(var i = 0; i < this._cardsInHands.length; i++) {
            var card = this._cardsInHands[i];
            card.x = this._startXPos + ((card.getCardWidth() + spacing) * i);
            card.y = this._yPos;
        }
    };

    p.getScore = function() {
        return this._score;
    };

    p.getNumOfTakenCards = function() {
        return this._cardsTakenArr.length;
    };

    p.reset = function() {
        this._cardsTakenArr = [];
        this._score = 0;
    };

    system.Player = Player;
})();