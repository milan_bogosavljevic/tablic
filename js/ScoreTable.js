this.system = this.system || {};
(function(){
    "use strict";

    var ScoreTable = function(){
        this.Container_constructor();
        this.initScoreTable();
    };

    var p = createjs.extend(ScoreTable,createjs.Container);


    p.initScoreTable = function () {
        console.log("ScoreTable Initialized...");
    };

    system.ScoreTable = createjs.promote(ScoreTable,"Container");
})();