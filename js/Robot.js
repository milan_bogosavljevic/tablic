this.system = this.system || {};
(function(){
    "use strict";

    var Robot = function(){
        this.Player_constructor();
        this.initRobot();
    };

    var p = createjs.extend(Robot , system.Player);

    p.initRobot = function () {
        console.log("Robot Initialized...");
        this._yPos = 20;
    };

    system.Robot = createjs.promote(Robot , "Player");
})();