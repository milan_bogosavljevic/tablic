
this.system = this.system || {};
(function(){
    "use strict";

    var Card = function(number , sign , image){
        this.Container_constructor();
        this.initCard(number , sign , image);
    };

    var p = createjs.extend(Card,createjs.Container);
    p._number = null;
    p._value = null;

    p.initCard = function (number , sign , image) {
        //console.log(number + " " + sign + " Card Initialized");

        this._number = number;

        switch (number){
            case 2:
                if(sign === "Spades"){
                    this._value = 1;
                }else{
                    this._value = 0;
                }
                break;
            case 10:
                if(sign === "Diamonds"){
                    this._value = 2;
                }else{
                    this._value = 1;
                }
                break;
            default:
                if(number > 10){
                    this._value = 1;
                }else{
                    this._value = 0;
                }
        }

        // make image and add child
        this.addChild(image);

        this.addEventListener("click" , this.onCardSelect);
    };

    p.onCardSelect = function (e) {
        console.log("Value " + e.target.parent._value);
        console.log("Number " + e.target.parent._number);
    };

    p.getCardWidth = function () {
        return this.getChildAt(0).sourceRect.width;
    };

    p.getValue = function() {
        return this._value;
    };

    system.Card = createjs.promote(Card,"Container");
})();